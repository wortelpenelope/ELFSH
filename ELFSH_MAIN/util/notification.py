import json

class Notification(object):
    def __init__(self,
                 body="",
                 title="ELFSH",
                 actions=(),
                 badge="",
                 icon="",
                 image="",
                 lang="",
                 renotify=False,
                 tag=None,
                 vibrate=(200, 200, 200),
                 data=""):
        self.body = body,
        self.title = title
        self.actions = actions,
        self.badge = badge,
        self.icon = icon,
        self.image = image,
        self.lang = lang,
        self.renotify = renotify,
        self.tag = tag,
        self.vibrate = vibrate,
        self.data = data

    def to_json(self):
        return json.dumps({
            "title": self.title,
            "options": {
                "actions": self.actions,
                "badge": self.badge,
                "body": self.body,
                "icon": self.icon,
                "image": self.image,
                "lang": self.lang,
                "tag": self.tag,
                "vibrate": self.vibrate,
                "data": self.data
            }
        })