import functools

from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404

from ELFSH_MAIN import models
from ELFSH_MAIN.util.localization import localize


def require_login(funk):
    @functools.wraps(funk)
    def _inner(request, *args):
        if not request.user.is_authenticated():
            return HttpResponseForbidden(localize(request, "login_required"))

        return funk(request, *args)

    return _inner


def require_housemembership(funk):
    @functools.wraps(funk)
    def _inner(request, house, *args):
        if not request.user.is_authenticated():
            return HttpResponseForbidden(localize(request, "login_required"))
        house = get_object_or_404(models.House, id=int(house))
        user = models.ELFSHUser.objects.get(user=request.user)
        try:
            models.HouseMembership.objects.get(house=house, user=user)
        except models.HouseMembership.DoesNotExist:
            return HttpResponseForbidden(localize(request, "login_unauthorised"))

        return funk(request, house, *args)
    return _inner
