from ELFSH_MAIN import models


def get_user_info(request, house, extended=False):
    users = models.HouseMembership.objects.filter(house=house)

    info = []
    for user in users:
        dat = {
            "name": user.user.user.username,
            "id": user.user.user.id,
            "value": user.money,
            "points": user.points,
            "is_me": user.user.user.id == request.user.id
        }

        if extended:
            dat["email"] = user.user.user.email
            dat["last_login"] = (user.user.user.last_login.date().isoformat() if
                                 user.user.user.last_login is not None else
                                 None)

        info.append(dat)

    info.sort(
        key=lambda x: x["name"]
    )

    info.sort(
        key=lambda x: -x["is_me"]
    )

    return info


def get_house_info(request, house: models.House):
    return {
        "id": house.id,
        "name": house.name,
        "closing_time": house.closingTime,
        "editable_days": house.editableDays,
        "timezone": house.timeZone
    }


def get_account_info(request, house):
    if request.user.is_authenticated():
        return {
            "id": request.user.id
        }
    else:
        return {
            "authenticated": False
        }