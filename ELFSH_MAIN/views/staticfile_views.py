from django.http import HttpResponse

import webpack_loader_splitting as webpack_loader


def serviceworker(request):
    with open(webpack_loader.utils.get_files('serviceworker')[0]["path"]) as f:
        content = f.read()

    return HttpResponse(
        content,
        content_type="application/javascript",
    )


def robots(request):
    with open("static/robots.txt") as f:
        content = f.read()

    return HttpResponse(content,
                        content_type="text/plain")