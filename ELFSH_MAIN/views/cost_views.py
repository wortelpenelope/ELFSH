import datetime

from django.http import (JsonResponse, HttpResponseBadRequest)
from django.views.decorators.http import require_POST, require_safe

from ELFSH_MAIN import models
from ELFSH_MAIN.util.localization import localize
from ELFSH_MAIN.util.userinfo import get_user_info, get_account_info
from ELFSH_MAIN.views.misc_views import convert_old_eats_to_costs

from ELFSH_MAIN.util.decorators import login_decorators, validation
from ELFSH_MAIN.schemas import misc_schema


def _get_costs(request, house, max=20):
    expenses = models.Expense.objects.filter(house=house).order_by("-date")[:max]
    user_info = get_user_info(request, house)
    out = []
    for expense in expenses:
        cost_pp = expense.get_costs_per_person()
        user_info_all = [{"cost": j["cost"],
                          "delta_points": j["points"],
                          "extra": j["extra"],
                          **i}
                         for i in user_info for j in cost_pp
                         if i["id"] == j["id"]]
        d = {
            "id": expense.id,
            "amount": expense.cost,
            "description": expense.description,
            "payed_by": expense.payed_by.id,
            "payed_for": {i.user.user.username: i.extra + 1
                          for i in models.ExpensePayedFor.objects.filter(expense=expense)},
            "costs": user_info_all,
            "date": expense.date.isoformat(),
            "deleted": expense.deleted,
            "editable": house.editableDays == 0 or (datetime.date.today() - expense.date).days < house.editableDays
        }
        out.append(d)
    return JsonResponse({
        "status": "success",
        "result": out,
        "length": len(expenses),
        "account_info": get_account_info(request, house),
        "user_info": user_info})


@login_decorators.require_housemembership
@validation.validate(misc_schema.get_costs_schema)
def get_costs(request, house, data):
    convert_old_eats_to_costs(request, house)

    return _get_costs(request, house, data.get("max", 10))


@require_POST
@login_decorators.require_housemembership
@validation.validate(misc_schema.add_cost_schema)
def add_cost(request, house, data):
    # Initial verification
    payed_for = [{"name": key, "value": value} for key, value in data["payed_for"].items() if value != 0]
    if len(payed_for) == 0:
        return HttpResponseBadRequest(localize(request, "cost_payees_not_empty"))

    try:
        payed_for_models = [
            models.HouseMembership.objects.get(user__user__username=i["name"], house=house)
            for i in payed_for
        ]
    except models.HouseMembership.DoesNotExist:
        return HttpResponseBadRequest("One payed for is invalid")

    try:
        payed_by_model = models.HouseMembership.objects.get(user__user__id=int(data["payed_by"]), house=house)
    except models.HouseMembership.DoesNotExist:
        return HttpResponseBadRequest("Payed by is invalid")

    all_models = payed_for_models + [payed_by_model]

    data_id = int(data["id"])
    if data_id == 0:
        cost = models.Expense(description=str(data["description"]),
                              house=house,
                              payed_by=payed_by_model.user,
                              cost=int(data["amount"]),
                              deleted=data["deleted"],
                              date=datetime.date.today()
                              )
        cost.save()

        for i in payed_for_models:
            if data["payed_for"][i.user.user.username] == 0:
                continue  # skip any 0 elements
            if data["payed_for"][i.user.user.username] - 1 < 0:
                return HttpResponseBadRequest("Extra eats must be at least 1")

            m = models.ExpensePayedFor(expense=cost, user=i.user, extra=data["payed_for"][i.user.user.username] - 1)
            m.save()
    else:
        try:
            cost = models.Expense.objects.get(id=data_id)
        except models.Expense.DoesNotExist:
            return HttpResponseBadRequest(localize(request, "cost_does_not_exist").format(repr(data_id)))

        cost.revert_expense(all_models=all_models, save=False)

        cost.description = str(data["description"])
        cost.house = house
        cost.payed_by = payed_by_model.user
        cost.cost = int(data["amount"])
        cost.deleted = bool(data["deleted"])

        cost.save()
    # assert len(cost.payed_for.all())>0

    cost.apply_expense(all_models=all_models, save=False)
    for model in all_models:
        model.save()

    return _get_costs(request, house, data.get("max", 20))
