import datetime

from django.conf import settings
from django.http.response import (JsonResponse, HttpResponseBadRequest,
                                  HttpResponseForbidden,
                                  HttpResponseNotFound)
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_POST, require_safe

from ELFSH_MAIN import models
from ELFSH_MAIN.util.localization import localize
from ELFSH_MAIN.util.userinfo import get_user_info, get_house_info, get_account_info

from ELFSH_MAIN.util.decorators import login_decorators, validation
from ELFSH_MAIN.schemas import misc_schema

MAX_DAYS = 14


# Create your views here.
@ensure_csrf_cookie
def main_page(request):
    return render(request, 'index.html', {
        "name": str(settings.STATICFILES_DIRS),
        "lang": request.COOKIES.get("lang", "en")[:3]
    })


def return_404(request):
    return HttpResponseNotFound("Page not found")


@login_decorators.require_login
def get_houses(request):
    if not request.user.is_authenticated():
        return HttpResponseForbidden(localize(request, "login_required"))

    user = get_object_or_404(models.ELFSHUser, user=request.user)
    houses = user.houses.all()

    return JsonResponse({"houses": [{"name": i.name, "id": i.id} for i in houses]})


def convert_old_eats_to_costs(request, house):
    eats = models.EatState.objects.filter(house=house, date__lt=datetime.date.today())
    if len(eats) == 0:
        return
    for eat in eats:
        if eat.eatState == 2:  # cooking
            share_day = [i for i in eats if i.date == eat.date and i.eatState != 0]
            if len(share_day) == 1:
                continue
                # don't clog up expenses if you are only paying for yourself
                # the net difference would be 0 anyway in this case
                # There is actually a unit test for this

            expense = models.Expense(
                description="[Error]" if eat.date is None else "_____",
                house=house,
                payed_by=eat.user,
                cost=0,
                deleted=False,
                is_meal=True,
                date=eat.date if eat.date is not None else datetime.date.today()
            )
            expense.save()

            for day in share_day:
                d = models.ExpensePayedFor(
                    user=day.user,
                    expense=expense,
                    extra=day.extraPeople
                )
                d.save()

            expense.apply_expense()

    for eat in eats:
        eat.delete()

    eats = models.EatState.objects.filter(house=house, date__lt=datetime.date.today())
    assert len(eats) == 0


@login_decorators.require_housemembership
@require_safe
def house_info(request, house):
    return JsonResponse({
        "user_info": get_user_info(request, house, extended=True),
        "house_info": get_house_info(request, house),
        "account_info": get_account_info(request, house)
    })


@login_decorators.require_housemembership
@require_POST
@validation.validate(misc_schema.set_house_info_schema)
def set_house_info(request, house, data):

    if len(data["name"]) > 20:
        return HttpResponseBadRequest(localize(request, "house_bad_name"))

    house.closingTime = data["closing_time"]
    house.name = data["name"]
    house.editableDays = data["editable_days"]
    house.timeZone = data["timezone"]
    house.save()

    return JsonResponse({"status": "success"})
