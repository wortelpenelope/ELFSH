import push_notifications.models as pushmodels
from django.http import JsonResponse, HttpResponseServerError
from django.views.decorators.http import require_POST
from ELFSH_MAIN.util.localization import localize
from ELFSH_MAIN.util.decorators import validation, login_decorators

import json
from ELFSH_MAIN import models
from ELFSH_MAIN.util.notification import Notification
from ELFSH_MAIN.schemas import push_schemas


@require_POST
@login_decorators.require_login
@validation.validate(
    push_schemas.register_push_schema
)
def registerPush(request, data):
    # returnvalue = webpush(data["info"],
    #                       "hello")
    #
    def makejson(v):
        return {
            i: str(getattr(v, i)) for i in dir(v) if not i.startswith("__")
        }

    name = (request.user.username + "_" + data["browser"] + "_(" + request.META.get("HTTP_USER_AGENT") + ")") \
        .upper().replace(" ", "_")
    #
    # rvalue = makejson(returnvalue)
    # request = makejson(returnvalue.request)
    if data["registration_id"]:
        try:
            device = pushmodels.WebPushDevice.objects.get(registration_id=data["registration_id"])

            device.name = name
            device.active = True
            device.user = request.user
            device.browser = data["browser"]
        except pushmodels.WebPushDevice.DoesNotExist:
            device = None
    else:
        device = None
    if device is None:
        device = pushmodels.WebPushDevice(
            name=name,
            active=True,
            user=request.user,
            registration_id=data["registration_id"],
            p256dh=data["p256dh"],
            auth=data["auth"],
            browser=data["browser"]
        )

        device.clean()

    device.save()

    elfshUser = models.ELFSHUser.objects.get(user=request.user)

    try:
        rvalue = device.send_message(
            Notification(
                localize(None, "push_test_message", lang=elfshUser.language),
                title=localize(None, "push_test_title", lang=elfshUser.language),
            ).to_json()
        )
    except Exception as ex:
        return HttpResponseServerError(json.dumps(makejson(ex)), content_type="application/json")

    return JsonResponse({"done": True, "rvalue": rvalue})
