from . import *

request_password_reset_schema = DictSchema(
    {
        "email": StringSchema(regex=".+@.+",
                              message="valitaion_email")
    }
)

apply_reset_token_schema = DictSchema(
    {
        "password": StringSchema(regex=".+", message="validation_password")
    }
)
