from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.House)
admin.site.register(models.ELFSHUser)
admin.site.register(models.HouseMembership)
admin.site.register(models.NotificationProcessed)
