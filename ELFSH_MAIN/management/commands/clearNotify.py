from ELFSH_MAIN import models
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'sends notifications'

    def handle(self, *args, **options):
        notifications = models.NotificationProcessed.objects.all()
        notifications.delete()


if __name__ == "__main__":
    print("Notifying...")
    Command().handle()
