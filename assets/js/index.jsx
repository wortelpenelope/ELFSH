/* this file is bootstrapping code */
import React from 'react';
import ReactDOM from 'react-dom';

/* Stuff for font awesome*/
import { library } from '@fortawesome/fontawesome-svg-core';
import { faStar, faEdit, faTrashAlt, faEye, faUtensils, faCircleNotch } from '@fortawesome/free-solid-svg-icons';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';

library.add(faStar, faEdit, faTrashAlt, faEye, faUtensils, faGitlab, faCircleNotch);

import {set_cookie, get_cookie} from './util/cookies.js';

if (!get_cookie("lang")){
    set_cookie("lang", "nl");
}
if (!localStorage.getItem("lang")){
    localStorage.setItem("lang", get_cookie("lang"))
}

/*stuff for react*/
//import App from external library
import {App} from './app.jsx';
ReactDOM.render(<App/>, document.getElementById('container'));