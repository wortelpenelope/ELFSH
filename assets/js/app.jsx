import React from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import {Userinfo} from './components/userinfo.jsx';

import {EatList} from './toplevel/lijst.jsx';
import {LoginScreen, LogoutScreen} from './toplevel/login.jsx';
import {HousesScreen} from './toplevel/houses.jsx';
import {Kosten} from './toplevel/kosten.jsx';
import {Settings} from './toplevel/settings.jsx';
import {InvitationScreen} from './toplevel/invitation.jsx';
import {Newhouse, Newhouse_sucess} from './toplevel/newhouse';

import {localize} from './util/localization.js';
import {AcceptResetPage, RequestResetPage} from "./toplevel/reset";
import {ErrorBoundary} from "./components/errorBoundary";
import {VerifyScreen} from "./toplevel/verify";

function renderWithProps(component, props) {
  return (router_props) => {
    return React.createElement(component, {...props, ...router_props})
  }
}

export class App extends React.Component {
  constructor() {
    super();
    this.state = {
      loggedIn: false,
      user: undefined
    };
  }

  onUserChange(loggedIn, user) {
    this.setState({
      loggedIn: loggedIn,
      user: user,
    });
  }

  render() {
    let attrs = {
      loggedIn: this.state.loggedIn,
      user: this.state.user,
      onUserChange: this.onUserChange.bind(this),
    };

    return (
      <BrowserRouter>
        <ErrorBoundary>
          <div className="main-content">
            <Userinfo loggedIn={this.state.loggedIn} user={this.state.user}
                      onUserChange={this.onUserChange.bind(this)}/>

            <Switch>
              <Route exact path="/" render={() => <Redirect to="/login"/>}/>
              <Route exact path="/login" render={renderWithProps(LoginScreen, attrs)}/>
              <Route exact path="/logout" render={renderWithProps(LogoutScreen, attrs)}/>
              /* for some reason double slash does not work*/
              <Route exact path="/:user_id(\d+)/houses" render={renderWithProps(HousesScreen, attrs)}/>
              <Route exact path="/:user_id(\d+)/list/:house_id(\d+)"
                     render={renderWithProps(EatList, attrs)}/>
              <Route exact path="/:user_id(\d+)/cost/:house_id(\d+)"
                     render={renderWithProps(Kosten, attrs)}/>
              <Route exact path="/:user_id(\d+)/settings/:house_id(\d+)"
                     render={renderWithProps(Settings, attrs)}/>
              <Route exact path="/invitation/:invitation_id"
                     render={props => <Redirect match={props.match} to={"/invitation/" +
                     props.match.params.invitation_id + "/start"}
                                                history={props.history}/>}

              />
              <Route exact path="/invitation/:invitation_id/:page"
                     render={renderWithProps(InvitationScreen, attrs)}/>
              <Route extact path="/reset/:code"
                     render={renderWithProps(AcceptResetPage, attrs)}/>
              <Route exact path="/reset"
                     render={renderWithProps(RequestResetPage, attrs)}/>

              <Route exact path="/new"
                     render={renderWithProps(Newhouse, attrs)}/>
              <Route exact path="/new/done"
                     render={renderWithProps(Newhouse_sucess, attrs)}/>
              <Route exact path="/verify/:code"
                     render={renderWithProps(VerifyScreen, attrs)}/>

              <Route exact render={() => ("404 - page not found")}/>
            </Switch>

            <div className="footer">
              {localize("footer_line_1")}<br/>
              {localize("footer_line_2")}
              <a href="https://gitlab.com/mousetail/ELFSH">
                <FontAwesomeIcon icon={["fab", "gitlab"]}/> gitlab</a>
            </div>
          </div>
        </ErrorBoundary>
      </BrowserRouter>
    );
  }
}