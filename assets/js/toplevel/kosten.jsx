import React from 'react';

import {request} from "../util/request";
import {Nav} from "../components/nav.jsx";
import {localize, formatMoney} from '../util/localization.js';
import {formatDate} from '../util/date.jsx';
import {Dropdown} from '../generic/dropdown.jsx';
import {MoneyInput} from '../generic/numberInput.jsx';
import {ErrorView} from '../components/error.jsx';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {FullscreenLoading, InlineLoading} from "../components/loading";

function submitExpenseChange(house_id, expense_id, description, amount, payed_by, payed_for, deleted, maxEntries) {
  return request("/api/new/" + house_id,
    {
      "id": expense_id,
      "description": description,
      "amount": amount,
      "payed_by": payed_by,
      "payed_for": payed_for,
      "deleted": deleted,
      "max": maxEntries
    }
  ).then(
    (data) => {
      if (data.hasOwnProperty("result") && data.hasOwnProperty("result") &&
        data.hasOwnProperty("user_info")) {
        return data;
      }
      else {
        throw new Error(localize("error_invalid_response"))
      }
    }
  )
}

class ExpensePerson extends React.Component {
  render() {
    return <div className={"expense-person" + (this.props.value === 0 ? " expense-person-gray" : "")}>
      <div className={"table-elem table-elem-add"}
           onClick={() => this.props.onChange(Math.max(this.props.value - 1, 0))}>-
      </div>
      <div className="expense-person-middle">{this.props.person}
        <span className={this.props.value === 0 ? "" : "green"}>×{this.props.value}</span>
      </div>
      <div className={"table-elem table-elem-add"} onClick={() => this.props.onChange(this.props.value + 1)}>+
      </div>
    </div>

    /*<label>
                        <input type="checkbox"
                               checked={this.props.value!==0}
                               onChange={event=>this.props.onChange(event.target.checked?1:0)}
                               />
                        {this.props.person}
                    </label>*/
  }
}

class Expense extends React.Component {
  constructor() {
    super();

    this.state = {
      "editing": false,
      "error": undefined,
      "loading": false,

      "old_expense": undefined
    }
  }

  submit(params = {}) {
    let pars = Object.assign({}, this.props.expense, params);
    this.setState({
      "loading": true
    });
    submitExpenseChange(
      this.props.house_id,
      pars.id,
      pars.description,
      pars.amount,
      pars.payed_by,
      pars.payed_for,
      pars.deleted,
      this.props.maxEntries
    ).then(
      data => {
        this.props.replaceData(data);
        this.setState(
          {
            "loading": false,
            "editing": false
          }
        )
      }
    ).catch(
      (error) => {
        console.log(error);
        this.setState({
          "error": error,
          "loading": false
        })
      }
    )


  }

  render() {
    if (this.props.expense.deleted) {
      return <div className="expense expense-hidden">
        <div className="expense-header">
          <div className="expense-header-name">{this.props.expense.description}</div>

          <ErrorView error={this.state.error}/>
          <div className="expense-header-item">{localize("cost_deleted")}</div>
          {
            this.props.expense.editable ?
              <div className="expense-buttons">
                            <span className="expense-buttons-button" onClick={
                              () => {
                                this.props.onChange("deleted", false);
                                this.submit({"deleted": false});
                              }
                            }><FontAwesomeIcon icon={"eye"}/></span>
              </div> : null
          }
        </div>
      </div>;
    }
    else {
      let buttonArea;
      if (this.state.loading) {
        buttonArea = <InlineLoading/>
      }
      else if (this.state.editing) {
        buttonArea = <div className="expense-header-item">
          <button onClick={this.submit.bind(this)}>
            {localize("cost_submit")}
          </button>
          <button onClick={
            () => {
              Object.keys(this.state.old_expense).forEach(
                (key) => {
                  this.props.onChange(key, this.state.old_expense[key]);
                }
              );
              this.setState({"editing": false});
            }
          }
          >
            {localize("cost_cancel")}</button>
        </div>
      }
      else if (!this.props.expense.editable) {
        buttonArea = null
      }
      else {
        buttonArea = (
          <div className="expense-header-item expense-buttons">
                        <span className="expense-buttons-button"
                              onClick={() => this.setState(
                                {
                                  "editing": true,
                                  //Make a shallow copy of the expense
                                  //It should not have any nested properties anyway
                                  //Will not work in IE
                                  "old_expense": Object.assign({}, this.props.expense)
                                }
                              )
                              }
                        >
                            <FontAwesomeIcon icon="edit"/>
                        </span>
            <span
              className="expense-buttons-button"
              onClick={() => {
                this.props.onChange("deleted", true);
                this.submit({"deleted": true});
              }
              }
            >
                        <FontAwesomeIcon icon="trash-alt"/>
                    </span>
          </div>)
      }


      return (
        <React.Fragment>
          <div className="expense">
            <div className="expense-header">
              <div className="expense-header-name">
                {this.state.editing ?
                  <input value={this.props.expense.description}
                         onChange={
                           event => this.props.onChange("description", event.target.value)}
                  />
                  :
                  this.props.expense.description}
              </div>

              <ErrorView error={this.state.error}/>
              <div className="expense-header-item">
                {formatDate(new Date(this.props.expense.date))}
              </div>
              <div className="expense-header-item">
                {this.state.editing ?
                  <MoneyInput
                    value={this.props.expense.amount}
                    onChange={
                      value => this.props.onChange("amount", value)}
                  /> :
                  formatMoney(this.props.expense.amount)}
              </div>
              {
                buttonArea
              }
            </div>
            {
              this.props.expense.costs.map(
                j => (
                  <div key={j.name} className={"expense-item" + (j.is_me ? " expense-item-me" : "")}>
                    <div className={"expense-item-name"}>{j.name}
                      {j.extra !== 0 ? <span className="green bold">{"×" + (j.extra + 1)}</span> : undefined}
                    </div>
                    <div className={j.cost > 0 ? "green" : j.cost < 0 ? "red" : ""}>{formatMoney(j.cost)}</div>
                    {
                      j.delta_points === 0 ? undefined :
                        <div className="expense-item-point">
                          <FontAwesomeIcon icon="utensils"/> {
                          j.delta_points > 0 ?
                            '+' + j.delta_points :
                            j.delta_points
                        }
                        </div>
                    }
                  </div>
                )
              )//<div style={{"flexGrow": 9999}}/>
            }

          </div>
          <hr/>
        </React.Fragment>
      );
    }
  }
}

class ExpenseEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      "error": undefined,
      "loading": false
    }
  }

  submitNewCost(event) {
    event.preventDefault();
    submitExpenseChange(
      this.props.house_id,
      0,
      this.props.params.description,
      this.props.params.amount,
      this.props.params.payed_by,
      this.props.params.payed_for,
      false,
      this.props.maxEntries
    ).then(
      (data) => {
        this.props.replaceData(data);
        this.setState({"error": undefined});
        this.props.onChange("description", "");
        this.props.onChange("amount", "");
        window.scroll(0, 0);
      }
    ).catch(
      (error) => {
        console.log(error);
        this.setState({"error": error});
      }
    )
  }

  render() {
    return <form>
      <h2>{localize("cost_add_expense")}</h2>
      <ErrorView error={this.state.error}/>
      <label className="inputLabel">{localize("cost_description")}
        <input value={this.props.params.description}
               onChange={(event) => this.props.onChange("description", event.target.value)}/>
      </label><br/>
      <label className="inputLabel">{localize("cost_amount")}
        <MoneyInput
          value={this.props.params.amount}
          onChange={value => this.props.onChange("amount", value)}
        />
      </label><br/>
      <label className="inputLabel">{localize("cost_payed_by")}
        <Dropdown value={this.props.params.payed_by}
                  options={this.props.user_info.map(
                    i => ({
                      "name": i.name + " (" + formatMoney(i.value) + ")",
                      "value": i["id"]
                    }))}
                  onChange={value => this.props.onChange("payed_by", value)}
        />
      </label><br/>
      <span>{localize("cost_payed_for")}</span>
      {
        Object.keys(this.props.params.payed_for).map(
          (key) => (
            <ExpensePerson key={key} person={key} value={this.props.params.payed_for[key]}
                           onChange={(value) => {
                             this.props.onChange(
                               "payed_for", {...this.props.params.payed_for, [key]: value}
                             )
                           }
                           }/>
          )
        )
      }
      <br/>
      <button
        onClick={this.submitNewCost.bind(this)}>{localize("cost_submit")}
      </button>
    </form>
  }
}

export class Kosten extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      "loading": true,
      "data": undefined,
      "costsResponse": undefined,
      "error": undefined,
      "maxEntries": 6,

      "new_field": {
        "amount": 0,
        "description": "",
        "payed_by": 1,
        "payed_for": {},
      }
    }
  }

  componentDidMount() {
    this.reload(this.state.maxEntries)
  }

  reload(max) {
    request("/api/costs/" + this.props.match.params.house_id + "/",
      {"max": max}
    ).then(
      (data) => {
        let payed_for = {};
        data.user_info.map(
          i => {
            payed_for[i.name] = 0;
          }
        );

        this.setState(
          {
            "data": data,
            "loading": false,
            "new_field": {
              "amount": 0,
              "description": "",
              "payed_by": data.account_info.id,
              "payed_for": payed_for,
            }
          }
        )
      }
    ).catch(error => {
      console.log(error);
      this.setState(
        {
          "loading": false,
          "error": error
        }
      )
    });
  }

  render() {
    return <React.Fragment>
      <Nav house_id={this.props.match.params.house_id}
           user_id={this.props.match.params.user_id}
           selected={"cost"}
      />
      {
        this.state.loading ? (
            <FullscreenLoading/>
          )
          : this.state.error ?
          <ErrorView error={this.state.error}/> :
          <React.Fragment>
            {
              this.state.data.result.map(
                i => (
                  //this is the ugliest nested function ever
                  <Expense key={i.id}
                           expense={i}
                           user_info={this.state.data.user_info}
                           house_id={this.props.match.params.house_id}
                           replaceData={(data) => this.setState({"data": data})}
                           maxEntries={this.state.maxEntries}
                           onChange={(property, value) => {
                             this.setState(
                               (state) => {
                                 let data = state.data;
                                 data.result.map(
                                   (item) => {
                                     if (item.id === i.id) {
                                       item[property] = value;
                                     }
                                     return item;
                                   }
                                 );
                                 return {"data": data};
                               }
                             )
                           }}
                  />
                )
              )
            }
            {
              this.state.maxEntries === this.state.data.result.length ?
                <button onClick={() => {
                  let entries = this.state.maxEntries + 12;
                  this.setState({"maxEntries": entries},
                    this.reload(entries));

                }
                }>Load more
                </button> : null
            }
            <ExpenseEdit user_info={this.state.data.user_info}
                         params={this.state.new_field}
                         house_id={this.props.match.params.house_id}
                         account_info={this.state.data.account_info}
                         onChange={(name, value) => this.setState((state) => (
                           {new_field: {...state.new_field, [name]: value}}))
                         }
                         maxEntries={this.state.maxEntries}
                         replaceData={(data) => this.setState({"data": data})}
            />
          </React.Fragment>
      }
    </React.Fragment>
  }
}