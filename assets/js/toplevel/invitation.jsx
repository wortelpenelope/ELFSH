import React from 'react';

import {Link} from 'react-router-dom';

import {get_cookie} from "../util/cookies";
import {localize} from "../util/localization.js";

import {LoginFragment} from './login.jsx';
import {LanguageSelect} from "./login";

import {request} from '../util/request.jsx';
import {ErrorView} from '../components/error.jsx';
import {FullscreenLoading} from "../components/loading";

class InvitationLoginStep extends React.Component {
    render() {
        return <LoginFragment
            extraMessage={this.props.data.user_exists?
                (localize("invitation_match_1")+this.props.data.email+
                 localize("invitation_match_2")):
                undefined
            }
            onSubmit={
                (data)=> {
                    if (!this.props.data.user_exists || data.user.email === this.props.data.email) {
                        this.props.redirect("confirm")
                    }
                    else {
                        this.props.redirect("logout")
                    }

                }
            }
            onUserChange={this.props.onUserChange}
        />
    }
}

class LogoutStep extends React.Component {
    constructor() {
        super();
        this.state = {
            "error": undefined
        }
    }

    signOut() {
        request(
            "/api/logout",
        ).then(
            ()=>{
                this.props.onUserChange(false, undefined);
                this.props.redirect("login")
            }
        ).catch(
            error=>this.setState(
                {"error": error}

            )
        )
    }

    render() {
        return <React.Fragment>
            <h1>{localize("invitation_logout_title")}</h1>
            <p>{localize("invitation_logout_1")+this.props.data.email+
                localize("invitation_logout_2")}</p>
            <p>{localize("invitation_logout_3")}</p>
            <ErrorView error={this.state.error}/>
            <button onClick={this.signOut.bind(this)}>{localize("invitation_logout")}</button>
        </React.Fragment>
    }
}

class InvitationRegisterStep extends React.Component {
    constructor() {
        super();

        this.state = {
            "username": "",
            "password": "",
            "password2": "",

            "error": ""
        }
    }

    submit(event) {
        event.preventDefault();

        if (this.state.password !== this.state.password2) {
            this.setState({"error":"passwords do not match"});
            return;
        }

        request(
            "/api/create_account/",
            {
                        "uuid": this.props.uuid,
                        "username": this.state.username,
                        "password": this.state.password,
                        "password2": this.state.password2
                    }

        ).then(
            (data)=> {
                if (data.status === "success") {
                    this.props.redirect("done")
                }
                else {
                    return Promise.reject(data.status)
                }
            }
        ).catch(
            (error)=>{
                console.log(error);
                this.setState({"error": error})
            }
        )
    }

    render() {
        return <form>
            <h1>Create account</h1>
            <LanguageSelect onChange={this.forceUpdate.bind(this)}/>
            <ErrorView error={this.state.error}/>
            <label>
                {localize("login_username")}:
                <input value={this.state.username}  onChange={(event)=>this.setState({"username": event.target.value})}/>
            </label><br/>
            <label>
                Email:
                <input readOnly value={this.props.data.email}/>
            </label><br/>
            <label>
                {localize("login_password")}:
                <input type="password"
                    value={this.state.password}
                    onChange={(event)=>this.setState({"password": event.target.value})}
                />
            </label><br/>
            <label>
                {localize("login_password_confirm")}:
                <input type="password"
                    value={this.state.password2}
                       onChange={(event)=>this.setState({"password2": event.target.value})}
                />
            </label>
            <button
                onClick={this.submit.bind(this)}
            >{localize("cost_submit")}</button>
        </form>
    }
}

class ConfirmScreen extends React.Component {
    constructor() {
        super();

        this.state = {
            "error": undefined
        }
    }

    submit() {
        request(
            "/api/accept_invitation",
            {
                        "uuid": this.props.uuid
                    }

        ).then(
            ()=>{
                this.props.redirect("done");
            }
        ).catch(
            error=>this.setState(
                {error: error}
            )
        )
    }

    render() {
        return <React.Fragment>
            <h1>{localize("invitation_confirm_welcome")}</h1>
            <p>
                {localize("invitation_confirm_1")+this.props.data.house_name
                +localize("invitation_confirm_2")}
            </p>
            <ErrorView error={this.state.error}/>
            <button
                onClick={this.submit.bind(this)}
            >{localize("invitation_confirm")}</button>
        </React.Fragment>
    }
}

class DoneScreen extends React.Component {
    render() {
        return <React.Fragment>
            <h1>{localize("invitation_done_title")}</h1>
            <Link to={"/0/houses"}>{localize("invitation_done_link")}</Link>
        </React.Fragment>
    }
}

export class InvitationScreen extends React.Component {
    constructor() {
        super();
        this.state = {
            "loading": true,
            "error": undefined,
            "data": undefined,
        }
    }

    redirect(subpage) {
        this.props.history.push("/invitation/"+this.props.match.params.invitation_id+"/"+subpage)
    }

    componentDidMount() {
        if (this.props.match.params.page === "done"){
            this.setState({"loading": false});
            return;
        }
        request("/api/verify_invitation",
            {"uuid":this.props.match.params.invitation_id}
        ).then(
            (data)=>{
                if (this.props.match.params.page !== "start"){
                    // Do nothing if already on a specific page
                }

                //Option 1: See below
                else if (data.user_exists && data.user_loggedin && data.user_same){
                    this.redirect("confirm")
                }
                //Option 2
                else if (data.user_exists && !data.user_loggedin){
                    this.redirect("login") //Will redirect to confirm next
                }
                //Option 3
                else if (data.user_exists && data.user_loggedin && !data.user_same){
                    this.redirect("logout") //Will redirect to login
                }
                //Option 4
                else if(!data.user_exists &&  !data.user_loggedin){
                    this.redirect("register") //Also option to log in
                }
                //Option 5
                else if (!data.user_exists && data.user_loggedin) {
                    this.redirect("confirm")
                }
                else {
                    throw new Error("Invalid option: "+data.user_exists+" "+data.user_loggedin+" "+data.user_same)
                }


                this.setState({
                    "data": data,
                    "loading": false,
                });
            }
        ).catch(
            (error)=>{
                console.log(error);
                this.setState(
                    {
                        "loading": false,
                        "error": ""+error
                    }
                )
            }
        )
    }

    render() {
        if (this.state.loading){
            return <FullscreenLoading/>
        }
        else if (this.state.error){
            return <ErrorView error={this.state.error}/>
        }
        else if (this.props.match.params.page === "login"){
            return <InvitationLoginStep
                redirect={this.redirect.bind(this)}
                data={this.state.data}
                uuid={this.props.match.params.invitation_id}
                onUserChange={this.props.onUserChange}
                />
        }
        else if (this.props.match.params.page === "register"){
            return <InvitationRegisterStep
                redirect={this.redirect.bind(this)}
                data={this.state.data}
                uuid={this.props.match.params.invitation_id}
            />
            //Load register page
        }
        else if (this.props.match.params.page === "logout"){
            return <LogoutStep
                redirect={this.redirect.bind(this)}
                data={this.state.data}
                uuid={this.props.match.params.invitation_id}
                onUserChange={this.props.onUserChange}
                />
            //Log out page
        }
        else if (this.props.match.params.page === "confirm"){
            //Confirm registration
            return <ConfirmScreen
                redirect={this.redirect.bind(this)}
                data={this.state.data}
                uuid={this.props.match.params.invitation_id}
            />
        }
        else if (this.props.match.params.page === "done"){
            return <DoneScreen
                redirect={this.redirect.bind(this)}
                data={this.state.data}
                uuid={this.props.match.params.invitation_id}
            />
        }
        else {
            return <div className="errorMessage">{localize("invitation_invalid_page")}{this.props.match.params.page}</div>
        }

        //There are at least 5 flows:
        //1: An account for the email exists, and the user is logged into that account
        //   Show only the confirm page
        //   Done!
        //2: An account for the email already exists, but the user is not logged in
        //   Prompt the user to log in, do not allow him to create a account
        //   Have a password recovery option
        //   TODO: password recovery option
        //3: An account for the email already exists, but the user is logged in as
        //   somebody else:
        //   Prompt the user to log out, then go to 2
        //   Perhaps have an option to merge accoutns? Seems complicated
        //   Done!
        //4: An account for the user does not exist, user is not logged in
        //   Prompt to create account, skip the confirmation page since joining a house is mandatory
        //   There must also be an option to log in to a different account, which will switch to
        //   flow #5
        //   (DONE!)
        //5: An account for the user does not exist, user is logged in
        //   Show confirmation page, prompt to change email
        //   Done!
    }
}