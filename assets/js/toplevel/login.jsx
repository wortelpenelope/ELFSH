import React from 'react';
import {Redirect} from 'react-router';
import {Link} from 'react-router-dom';
import {get_cookie, set_cookie} from "../util/cookies";
import {request} from '../util/request';

import {localize, setLanguage} from '../util/localization.js';
import {FullscreenLoading} from "../components/loading";

export class LanguageSelect extends React.Component {
  setLanguage(lang) {
    setLanguage(lang);
    this.props.onChange(lang);
  }

  render() {
    return <div className="language-select">
      <img src="https://cdn.rawgit.com/hjnilsson/country-flags/master/svg/nl.svg"
           onClick={() => this.setLanguage("nl")}
      />
      <img src="https://cdn.rawgit.com/hjnilsson/country-flags/master/svg/gb.svg"
           onClick={() => this.setLanguage("en")
           }
      />
    </div>
  }
}

export class LoginFragment extends React.Component {
  constructor() {
    super();
    this.state = {
      "loading": false,
      "username": "",
      "password": "",
    }
  }

  handleSubmit(event) {

    console.log(event);
    event.preventDefault();
    this.setState({"loading": true});
    fetch("/api/login/",
      {
        "method": "POST",
        "body": JSON.stringify({
          "username": this.state.username,
          "password": this.state.password,
        }),
        "credentials": "same-origin",
        "headers": {
          'Content-Type': 'application/json',
          'X-CSRFToken': get_cookie('csrftoken'),
          'accept': 'application/json'
        }
      }
    ).then(
      response => {
        if (!response.ok) {
          throw new Error(response.statusText);
        }
        return response
      }
    ).then(
      r => r.json()
    ).then(
      data => {
        if (data["status"] === "error") {
          throw new Error(data["error"]);
        }
        else if (data["status"] === "success") {
          return data;
        }
        else {
          throw new Error("Ill-formatted response: " + JSON.stringify(data));
        }
      }
    ).then(
      data => {
        this.props.onUserChange(
          true,
          data.user
        );
        this.props.onSubmit(data);
      }
    ).catch(
      error => this.setState({"error": "" + error, "loading": false})
    )
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }


  render() {
    if (this.state.loading) {
      return <FullscreenLoading/>
    }
    else {
      return <React.Fragment>
        <h1>{localize("login_welcome")}</h1>
        <LanguageSelect onChange={this.props.forceUpdate ? this.props.forceUpdate : () => this.forceUpdate()}/>
        <p>{localize("login_please")}</p>
        {this.props.extraMessage ? <p>{this.props.extraMessage}</p> : undefined}
        <form onSubmit={this.handleSubmit.bind(this)}>
          {
            this.state.error ? <p>{this.state.error}</p> : undefined
          }
          <label className="inputLabel">{localize("login_username")}
            <input
              name={"username"}
              value={this.state.username}
              onChange={this.handleInputChange.bind(this)}
            />
          </label><br/>

          <label className="inputLabel">{localize("login_password")}
            <input name={"password"}
                   type={"password"}
                   value={this.state.password}
                   onChange={this.handleInputChange.bind(this)}
            />
          </label><br/>

          <button type={"submit"}>{localize("login_login")}</button>
          <br/>
          <Link to={'/new'}>{localize('login_create_account')}</Link><br/>
          <Link to={"/reset"}>{localize("login_forgot_password")}</Link>
        </form>
      </React.Fragment>
    }
  }
}

export class LoginScreen extends React.Component {

  render() {
    if (this.props.loggedIn) {
      return <Redirect to={"/" + this.props.user.id + "/houses"}/>
    }
    return <>
      <LoginFragment
        onSubmit={(data) => this.props.history.push("/" + data.user.id + "/houses/")}
        forceUpdate={() => this.forceUpdate()}
        onUserChange={this.props.onUserChange}
      />
      <h1>{localize("promo_title")}</h1>
      <div className={"text-container"}>
        <p>{localize("promo_p_1")}</p>
        <h2>{localize("promo_header_2")}</h2>
        <img src={"/static/images/promo/lijst.png"} className="img-left"/>
        <p>{localize("promo_p_3")}</p>
        <h2>{localize("promo_header_4")}</h2>
        <img src={"/static/images/promo/cost.png"} className="img-right"/>
        <p>{localize("promo_p_5")}</p>
        <h2>{localize("promo_header_6")}</h2>
        <img src={"/static/images/promo/user.png"} className="img-left"/>
        <p>{localize("promo_p_7")}</p>
        <h2>{localize("promo_header_8")}</h2>
        <p>{localize("promo_p_9")}</p>
      </div>
      <h1>{localize("promo_faq")}</h1>
      <div className={"text-container"}>
        <p>{localize("promo_faq_1")}</p>
        <h2>{localize("promo_faq_q_1")}</h2>
        <p>{localize("promo_faq_a_1")}</p>
        <ul>
          {
            localize("promo_faq_a_1_b").map(
              (i, j) => <li key={j}>{i}</li>
            )
          }

        </ul>
        <h2>{localize("promo_faq_q_2")}</h2>
        <p>{localize("promo_faq_a_2")}</p>
      </div>
    </>
  }
}

export class LogoutScreen extends React.Component {
  componentWillMount() {
    this.props.onUserChange(false, undefined);
    request(
      "/api/logout", {}
    ).then(
      () => this.props.history.push("/login")
    ).catch(
      error => console.log(error)
    );
  }

  render() {
    return <div>Loading...</div>
  }
}