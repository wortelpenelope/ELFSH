import React from 'react'

import {request} from '../util/request'
import {urlBase64ToUint8Array} from "../notifications/util";
import {localize} from '../util/localization';
import {ValidationError, ValidationMessage} from '../generic/validationError';
import {ErrorView} from "../components/error";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index.es";
import {FullscreenLoading} from "../components/loading";
import {Link} from "react-router-dom";

export class Newhouse extends React.Component {
  constructor() {
    super();

    this.state = {
      captcha: null,

      error: null,

      captcha_text: "",
      housename: "",
      username: "",
      password: "",
      password2: "",
      email: "",

      licence1: false,
      licence2: false,
      licence4: false,
      licence5: false,

      captcha_validation: "",
      housename_validation: "",
      username_validation: "",
      password_validation: "",
      password2_validation: "",
      email_validation: "",
      licence_validation: ""
    }
  }

  componentDidMount() {
    request(
      "/api/captcha"
    ).then(
      (data) => {
        let blob = new Blob(
          [urlBase64ToUint8Array(data.data)]
        );
        let url = URL.createObjectURL(blob);
        this.setState(
          {
            captcha: url,
            captcha_id: data.id,
          }
        )
      }
    )
  }

  componentWillUnmount() {
    if (this.state.captcha) {
      URL.revokeObjectURL(this.state.captcha);
    }
  }

  validate() {
    let ok = true;

    if (this.state.captcha_text === "") {
      this.setState(
        {
          captcha_validation: localize("newh_captcha_validation")
        }
      );
      ok = false;
    } else {
      this.setState({
        captcha_validation: ""
      })
    }
    if (this.state.username === "") {
      this.setState(
        {
          username_validation: localize("newh_username_validation")
        }
      );
      ok = false;
    } else {
      this.setState({
        username_validation: ""
      })
    }
    if (this.state.housename === "") {
      this.setState(
        {
          housename_validation: localize("newh_housename_validation")
        }
      );
      ok = false;
    } else {
      this.setState({
        housename_validation: ""
      })
    }
    if (this.state.password === "") {
      this.setState(
        {
          password_validation: localize("newh_password_validation")
        }
      );
      ok = false;
    } else {
      this.setState({
        password_validation: ""
      })
    }
    if (this.state.password2 === "" && this.state.password !== "") {
      this.setState(
        {
          password2_validation: localize("newh_password2_validation")
        }
      );
      ok = false;
    } else if (this.state.password2 !== this.state.password) {
      this.setState(
        {
          password2_validation: localize("newh_password2_validation_nomatch")
        }
      );
      ok = false;
    } else {
      this.setState(
        {
          password2_validation: ""
        }
      )
    }

    if (this.state.email.indexOf('@') === -1) {
      this.setState(
        {
          email_validation: localize("newh_email_validation")
        }
      );
      ok = false;
    } else {
      this.setState({
        email_validation: ""
      })
    }

    if (!(this.state.licence1 && this.state.licence2 &&
      this.state.licence4 && this.state.licence5)) {
      this.setState(
        {
          licence_validation: localize(
            "newh_licence_validation"
          )
        }
      );
      ok = false;
    } else {
      this.setState(
        {
          licence_validation: ""
        }
      )
    }
    return ok;
  }

  submit() {
    if (!this.validate()) {
      return;
    }
    this.setState(
      {error: undefined}
    );
    request(
      '/api/new_house',
      {
        'captcha_id': this.state.captcha_id,
        'name': this.state.housename,
        'captcha': this.state.captcha_text,
        'userinfo': {
          'username': this.state.username,
          'password': this.state.password,
          'email': this.state.email
        }
      }
    ).then(
      (data) => this.props.history.push(
        '/new/done',
        {
          'reply_email': data['reply_email']
        }
      )
    ).catch(
      (err) => {
        window.scrollTo(0, 0);
        this.setState(
          {error: err}
        );
      }
    );
  }

  render() {
    if (this.state.captcha === null) {
      return <FullscreenLoading/>;
    }

    return <div>
      <ErrorView error={this.state.error}/>
      <h1>{localize('newh_welcome')}</h1>
      <p>{localize('newh_captcha_title')}</p>
      <img src={this.state.captcha}/>
      <ValidationMessage error={this.state.captcha_validation}/>
      <label className={"inputLabel"}>
        {localize('newh_captcha_label')}
        <input value={this.state.captcha_text} onChange={
          ev => this.setState({captcha_text: ev.target.value})}/>
      </label>
      <h2>{localize('newh_basic_title')}</h2>

      <ValidationMessage error={this.state.housename_validation}/>
      <label className={"inputLabel"}>
        {localize('newh_house_name')}
        <input value={this.state.housename}
               onChange={
                 (ev) => this.setState(
                   {housename: ev.target.value}
                 )
               }
        />
      </label>

      <h2>{localize('newh_personal_title')}</h2>
      <ValidationMessage error={this.state.username_validation}/>
      <label className="inputLabel">
        {localize('login_username')}
        <input value={this.state.username}
               onChange={
                 (ev) => this.setState(
                   {username: ev.target.value}
                 )
               }
        />
      </label>


      <ValidationMessage error={this.state.password_validation}/>
      <label className="inputLabel">
        {localize('login_password')}
        <input type="password" value={this.state.password}
               onChange={(ev) => this.setState({
                 password: ev.target.value
               })}
        />
      </label>


      <ValidationMessage error={this.state.password2_validation}/>
      <label className="inputLabel">
        {localize('login_password_confirm')}
        <input type="password" value={this.state.password2}
               onChange={(ev) => this.setState({
                 password2: ev.target.value
               })}
        />
      </label>


      <ValidationMessage error={this.state.email_validation}/>
      <label className="inputLabel">
        {localize('settings_user_email')}
        <input type="email" value={this.state.email}
               onChange={(ev) => this.setState({
                 email: ev.target.value
               })}/>
      </label>

      <h2>{localize('newh_licence_title')}</h2>

      <ValidationMessage error={this.state.licence_validation}/>
      <label className="inputLabel">
        <input type="checkbox" checked={this.state.licence1} onChange={
          (event) => this.setState({licence1: event.target.checked})
        }/>{localize('newh_licence_1')} </label>
      <label className="inputLabel">
        <input type="checkbox" checked={this.state.licence2} onChange={
          (event) => this.setState({licence2: event.target.checked})}/>{
        localize('newh_licence_2')
      }</label>
      <label className="inputLabel">
        <input type="checkbox" checked={this.state.licence4} onChange={
          (event) => this.setState({licence4: event.target.checked})}/>{
        localize('newh_licence_4')
      }</label>
      <label className="inputLabel">
        <input type="checkbox" checked={this.state.licence5} onChange={
          (event) => this.setState({licence5: event.target.checked})}/>{
        localize('newh_licence_5')
      }</label>

      <button onClick={this.submit.bind(this)}>{localize('cost_submit')}</button><br/>
      <Link to={'/login'}>{localize('back')}</Link>
    </div>
  }
}

export class Newhouse_sucess extends React.Component {
  render() {
    return <div>
      <h1>{localize("newh_accept_title")}</h1>
      <p>{localize("newh_accept_text") + this.props.location.state.reply_email}</p>
    </div>
  }
}

