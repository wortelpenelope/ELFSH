import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import {formatDate} from '../util/date.jsx';
import {formatMoney, localize} from '../util/localization.js';
import {Nav} from '../components/nav.jsx';
import {Modal} from '../generic/modal.jsx';
import {request} from '../util/request.jsx';
import {ErrorView} from '../components/error.jsx';
import {FullscreenLoading} from "../components/loading";


class TableElement extends React.Component {
  constructor() {
    super();

    this.state = {
      "hover": false
    }
  }

  render() {
    return (
      <React.Fragment>
                <span onClick={
                  () => {
                    this.props.onChange({"state": (this.props.elem.state + 1) % 3, "extra": this.props.elem.extra})
                  }
                } className={"table-elem table-elem-" + this.props.elem.state}>
                    {this.props.elem.state === 0 ? "x" : this.props.elem.state === 1 ? "e" :
                      this.props.elem.state === 2 ? "k" : "?"}
                </span>

        {
          this.props.elem.extra >= 1 ?
            <span className={"table-elem table-elem-delete"}
                  onClick={() => this.props.onChange(
                    {"state": this.props.elem.state, "extra": this.props.elem.extra - 1})}
                  onMouseEnter={() => this.setState({"hover": true})}
                  onMouseLeave={() => this.setState({"hover": false})}
            >+{this.state.hover ? this.props.elem.extra - 1 : this.props.elem.extra}</span>
            : undefined

        }

        {
          this.props.elem.state === 0 ? undefined :
            <span className={"table-elem table-elem-add"}
                  onClick={() => this.props.onChange({
                    "state": this.props.elem.state,
                    "extra": this.props.elem.extra + 1
                  })}
            >
                            +
                        </span>
        }
      </React.Fragment>)
  }


}

class TableRow extends React.Component {
  render() {
    return <tr>
      <td>{formatDate(this.props.date)}</td>
      {
        this.props.users.map(
          (i) => <td key={i.id}>
            <TableElement
              user={i}
              elem={this.props.value[i.id]}
              onChange={(value) => {
                this.props.onChange(i.id, this.props.day, this.props.date, value)
              }
              }/>
          </td>)
      }

    </tr>
  }
}

// Programming today is a race between software engineers striving to build bigger and better idiot-proof programs, and
// the universe trying to produce bigger and better idiots. So far, the universe is winning. - Rick Cook

export class EatList extends React.Component {
  constructor() {
    super();
    this.state = {
      data: undefined,
      error: undefined,
      loading: true,
      modal: false,
      date: new Date()
    };
  }

  buttonClickEventHandler(person, day, date, value) {
    let d = new Date();
    let h = Number(this.state.data.house_info.closing_time.substr(0, 2));
    let m = Number(this.state.data.house_info.closing_time.substr(3, 2));
    console.log("d=", d, "h=", h, "m=", m, "day= " + day + " date=" + date);
    if (value["state"] === 1 && day === 0 && (d.getHours() > h || (d.getHours() === h && d.getMinutes() > m))) {
      this.setState(
        {
          "modal": true,
          "click_info": {
            "person": person,
            "day": day,
            "date": date,
            "value": value
          }
        }
      )
    }
    else {

      this.sendRequest(person, day, date, value);
    }
  }

  sendRequest(person, day, date, value) {
    request(
      `/api/update/${this.props.match.params.house_id}/${person}/${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`,
      {
        "state": value["state"],
        "extra": value["extra"]
      }).catch(
      error => {
        console.log(error);
        this.setState({"error": error});
      }
    );

    this.setState(
      state => {
        let t = state["data"];
        t["table"][day][person] = value;
        return {"data": t};
      }
    );
  }


  componentDidMount() {
    request("/api/future_days/" + this.props.match.params.house_id + "/"
    ).then(
      function (r) {
        this.setState(
          {
            "data": r,
            "loading": false,
            "error": undefined,
          });
      }.bind(this)
    ).catch(
      reason => {
        console.error(reason);
        this.setState({
          "error": reason,
          "loading": false
        })
      }
    );
  }

  render() {


    return (
      <React.Fragment>
        <Nav house_id={this.props.match.params.house_id}
             user_id={this.props.match.params.user_id}
             selected={"lijst"}
        />

        <ErrorView error={this.state.error}/>

        {
          this.state.loading ?
            <FullscreenLoading/> : undefined
        }

        {
          this.state.data ? <div className="table-container">
            <table cellSpacing="0">
              <tbody>
              <tr className="table-heading">
                <th/>
                {
                  this.state.data.users.map(
                    f => <th key={f.id} className={f.is_me ? "" : "th-disabled"}>
                      <div className="table-person-name">{"" + f.name}</div>
                    </th>
                  )
                }
              </tr>
              <tr className="table-heading">
                <th>{localize("list_money")}</th>
                {
                  this.state.data.users.map(
                    f => <th key={f.id}
                             className={f.is_me ? "" : "th-disabled"}>{formatMoney(f.value)}</th>
                  )
                }
              </tr>
              <tr className="table-heading">
                <th>{localize("list_point")} <FontAwesomeIcon icon="utensils"/></th>
                {
                  this.state.data.users.map(
                    f => <th key={f.id}
                             className={f.is_me ? "" : "th-disabled"}>{"" + f.points}</th>
                  )
                }
              </tr>
              {this.state.data.table.map(
                (f, i) => <TableRow value={f}
                                    date={new Date(new Date(this.state.data.date) - (-1000 * 60 * 60 * 24 * i))}
                                    day={i}
                                    users={this.state.data.users}
                                    key={i}
                                    onChange={this.buttonClickEventHandler.bind(this)}
                />)
              }
              </tbody>
            </table>
          </div> : undefined

        }
        {
          this.state.modal ? <Modal>
            <p>
              {localize("list_confirm")}
            </p>
            <button onClick={
              () => {
                this.sendRequest(
                  this.state.click_info.person,
                  this.state.click_info.day,
                  this.state.click_info.date,
                  this.state.click_info.value
                );
                this.setState({"modal": false})
              }
            }>{localize("list_confirm_ok")}
            </button>
            <button onClick={() => this.setState({"modal": false})}>
              {localize("list_confirm_cancel")}
            </button>
          </Modal> : undefined
        }
      </React.Fragment>
    );
  }
}