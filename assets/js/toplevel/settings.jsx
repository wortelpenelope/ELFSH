import React from 'react';

import {Nav} from '../components/nav.jsx';
import {localize} from '../util/localization.js';
import {formatDate} from "../util/date.jsx";
import {Dropdown} from "../generic/dropdown.jsx"
import {request} from "../util/request.jsx";
import {ErrorView} from "../components/error.jsx";
import {timezones} from "../util/timezones";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {FullscreenLoading} from "../components/loading";

class UserConfig extends React.Component {
    render() {
        return <div className="settings-user-info">
            <div>
                <div className="settings-user-info-label">{localize("settings_user_name")}</div>
                <div className="settings-user-info-value">{this.props.user.name}</div>
            </div>
            <div>
                <div className="settings-user-info-label">{localize("settings_user_email")}</div>
                <div className="settings-user-info-value">{this.props.user.email}</div>
            </div>
            <div>
                <div className="settings-user-info-label">{localize("settings_user_lastlogin")}</div>
                <div className="settings-user-info-value">
                    {this.props.user.last_login === null ?
                        localize("setting_never") :
                        formatDate(new Date(this.props.user.last_login))}
                </div>
            </div>
            <div>
                <div className="settings-user-info-label">{localize("settings_user_remove")}</div>
                <div>
                    <a href={""}>{localize("settings_user_remove")}</a>
                </div>
            </div>
        </div>
    }
}

class InvitationForm extends React.Component {
    constructor() {
        super();
        this.state = {
            "loading": false,
            "name": "",
            "email": "",
            "language": localStorage.getItem("lang"),
            "error": undefined
        }
    }

    submit(event) {
        event.preventDefault();
        this.setState(
            {"loading": true}
        );
        request("/api/send_invitation/" + this.props.house_id,
            {
                "name": this.state.name,
                "email": this.state.email,
                "lang": this.state.language
            }
        ).then(
            (data) => {
                this.setState({
                    "name": "",
                    "email": "",
                    "loading": false,
                    "error": undefined,
                })
            }
        ).catch(
            (error) => this.setState({"error": error, "loading": false})
        )
    }

    render() {
        return <form>
            <ErrorView error={this.state.error}/>
            <label className="inputLabel">{localize("settings_user_name")}
                <input value={this.state.name}
                       onChange={(event) => this.setState({"name": event.target.value})}
                />
            </label><br/>
            <label className="inputLabel">{localize("settings_user_email")}
                <input
                    type="email"
                    value={this.state.email}
                    onChange={(event) => this.setState({"email": event.target.value})}
                />
            </label><br/>
            <label className="inputLabel">{localize("setting_user_language")}
                <Dropdown value={this.state.language}
                          options={
                              [
                                  {"name": localize("setting_lang_english"), "value": "en"},
                                  {"name": localize("setting_lang_dutch"), "value": "nl"}
                              ]
                          }
                          onChange={(value) => this.setState({"language": value})}
                />
            </label><br/>
            {
                this.state.loading ? <div><FontAwesomeIcon icon="star" spin/>{localize("loading")}</div> :
                    <button
                        onClick={this.submit.bind(this)}
                    >
                        {localize("settings_user_send")}
                    </button>
            }
        </form>
    }
}

class GeneralSettingsScreen extends React.Component {
    constructor() {
        super();

        this.state = {
            "loading": false,
            "error": undefined,
        }
    }

    submit(event) {
        event.preventDefault();
        if (this.state.loading) {
            return;
        }

        this.setState(
            {
                "loading": true,
                "error": undefined
            }
        );

        request("/api/set_house_info/" + this.props.house_info.id,
            {
                "name": this.props.house_info.name,
                "closing_time": this.props.house_info.closing_time,
                "editable_days": Number.parseInt(this.props.house_info.editable_days),
                "timezone": this.props.house_info.timezone
            }
        ).then(
            (response) => this.setState({"loading": false})
        ).catch(
            (error) => {
                console.log(error);
                this.setState(
                    {
                        "error": error,
                        "loading": false
                    }
                )
            }
        )

    }

    toDropdownElement(value, pre=[]){
        let keys = Object.keys(value);
        keys.sort();
        let cf = (i,j)=>(j?(i+"/"+j):i);
        return keys.map(
            (i)=>{
                let pre2=pre.concat([i]);
                if (Object.keys(value[i]).length === 0) {
                    return {"value": (cf).apply(null, pre2), "name": i.replace("_", " ")}
                } else {
                    return {
                        "value": (cf).apply(null, pre2),
                        "name": i.replace("_", " "),
                        "type": "category",
                        "contents": this.toDropdownElement(value[i], pre2)
                    }
                }
            }
        )
    }

    render() {
        return <form>
            {
                this.state.loading ? <div>{localize("loading")}</div> : undefined
            }
            <ErrorView error={this.state.error}/>
            <label className="inputLabel">
                {localize("settings_name")}
                <input value={this.props.house_info.name}
                       onChange={(event) => this.props.setHouseInfo("name", event.target.value)}
                />
            </label>
            <label className="inputLabel">
                {localize("settings_endtime")}
                <input pattern="\d\d:\d\d"
                       value={this.props.house_info.closing_time}
                       onChange={(event) => this.props.setHouseInfo("closing_time", event.target.value)}/>
            </label>
            <label className="inputLabel">
                {localize("settings_dayseditable")}
                <input type="number"
                       min="0"
                       max="60"
                       value={this.props.house_info.editable_days}
                       onChange={(event) => this.props.setHouseInfo("editable_days", event.target.value)}
                />
            </label>
            <label className="inputLabel">
                {localize("settings_timezone")}
                <Dropdown
                    value={this.props.house_info.timezone}
                    options={
                        this.toDropdownElement(timezones)
                    }
                    onChange={(value) => this.props.setHouseInfo("timezone", value)}
                />
            </label>
            <button onClick={this.submit.bind(this)}>
                {localize("cost_submit")}
            </button>
        </form>
    }


}

export class Settings extends React.Component {
    constructor() {
        super();

        this.state = {
            "loading": true,
            "error": undefined
        }
    }

    setHouseInfo(info, value) {
        this.setState(
            state => {
                state["data"]["house_info"][info] = value;
                return {
                    "data":
                        state["data"]
                };
            }
        );
    }

    componentDidMount() {
        request("/api/info/" + this.props.match.params.house_id
        ).then(
            //TODO: do something usefull with the data
            data => this.setState(
                {
                    loading: false,
                    data: data,
                    error: undefined
                }
            )
        ).catch(
            error => {
                this.setState(
                    {
                        loading: false,
                        error: error,
                        data: undefined
                    }
                );
                console.error(error);
            }
        )
    }

    render() {
        return <React.Fragment>
            <Nav house_id={this.props.match.params.house_id}
                 user_id={this.props.match.params.user_id}
                 selected={"settings"}
            />
            {this.state.loading ? <FullscreenLoading/> :
                this.state.error ? <ErrorView error={this.state.error}/> :
                    <React.Fragment>

                        < h1> {localize('settings_title') + " " + this.state.data.house_info.name}</h1>
                        <h2>{localize('settings_general')}</h2>


                        <GeneralSettingsScreen
                            house_info={this.state.data.house_info}
                            setHouseInfo={this.setHouseInfo.bind(this)}
                        />
                        <h2>{localize('settings_users')}</h2>
                        <div className="settings-user-info-container">
                            {
                                this.state.data.user_info.map(
                                    (i) => <UserConfig user={i} key={i.id}/>
                                )
                            }
                        </div>
                        <h2>{localize('settings_invitations')}</h2>
                        <InvitationForm house_id={this.props.match.params.house_id}/>
                    </React.Fragment>
            }
        </React.Fragment>
    }
}