import React from 'react';
import {request} from "../util/request";
import {ErrorView} from "../components/error";


export class VerifyScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      error: false,
    }
  }

  componentDidMount() {
    request(
      `/api/verify/${this.props.match.params.code}`,
      {}
    ).then(
      (data) => {
        this.props.onUserChange(
          true,
          data.user
        );
        this.props.history.push(
          `/0/list/${data.house_id}`
        )
      }
    ).catch(
      (error) => this.setState({
        error: error,
        loading: false
      })
    )
  }

  render() {
    return <div>
      {
        this.state.loading ? <h1>Loading...</h1> : null
      }
      {
        this.state.error ? <ErrorView error={this.state.error}/> : null
      }
    </div>
  }
}