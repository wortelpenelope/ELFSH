import React from 'react';
import {localize} from '../util/localization';
import {request} from '../util/request';
import {ErrorView} from "../components/error";
import {Link} from "react-router-dom";

export class RequestResetPage extends React.Component {
  constructor() {
    super();

    this.state = {
      email: "",
      error: "",
      done: false,
      loading: false,
    }
  }

  submit(event) {
    event.preventDefault();
    this.setState(
      {"loading": true}
    );

    request(
      "/api/reset/request",
      {
        "email": this.state.email
      }
    ).then(
      () => this.setState({"done": true, "loading": false})
    ).catch(
      (error) => this.setState({"error": error, "loading": false})
    )
  }

  render() {
    if (this.state.done) {
      return <div>Done!</div>
    }
    else {
      return <form>
        <ErrorView error={this.state.error}/>
        <h1>{localize("reset_title")}</h1>
        <div className={"text-container"}>
          {localize("reset_explain")}
        </div>
        <label className="inputLabel">
          Email:
          <input type="email" value={this.state.email}
                 onChange={(event) => this.setState({email: event.target.value})}
                 disabled={this.state.loading}
          />
        </label>

        <button onClick={this.submit.bind(this)}>{localize("cost_submit")}</button><br/>
        <Link to={'/login'}>{localize('back')}</Link>
      </form>
    }
  }
}

export class AcceptResetPage extends React.Component {
  constructor() {
    super();

    this.state = {
      "password": "",
      "password2": "",
      "loading": false,
      "error": undefined,
    }
  }

  componentDidMount() {
    request(
      "/api/reset/verify/" + this.props.match.params.code
    ).catch(
      (error) => this.setState({error: error})
    )
  }

  submit(event) {
    event.preventDefault();

    this.setState(
      {
        loading: true
      }
    );

    if (this.state.password !== this.state.password2) {
      this.setState(
        {"error": "Passwords do not match"}
      );
      return;
    }


    request(
      "/api/reset/apply/" + this.props.match.params.code,
      {
        "password": this.state.password
      }
    ).then(
      () => this.props.history.push(
        "/login"
      )
    ).catch(
      (error) => {
        this.setState(
          {
            "error": error
          }
        )
      }
    )
  }

  render() {
    return <form>
      <ErrorView error={this.state.error}/>
      <label className="inputLabel">
        New password:
        <input type="password" value={this.state.password}
               onChange={(event) => this.setState({password: event.target.value})}/>
      </label>
      <label className="inputLabel">
        Confirm password:
        <input type="password" value={this.state.password2}
               onChange={(event) => this.setState({password2: event.target.value})}/>
      </label>

      <button onClick={this.submit.bind(this)}>{localize("cost_submit")}</button>
    </form>
  }
}