import React from 'react';

export class HousesScreen extends React.Component {

    componentDidMount() {
        fetch("/api/houses/",
            {
                credentials: "same-origin"
            }
        ).then(
            (request)=>{
                if (!request.ok){
                    if (request.status===401){
                        throw new Error("not logged in");
                    }
                    else {
                        throw new Error(request.statusText);
                    }
                }
                return request
            }
        ).then(
            r=>r.json()
        ).then(
            (data)=>{
                if (data.houses.length === 1){
                    console.log(this.props.match);
                    this.props.history.push("/"+this.props.match.params.user_id+
                        "/list/"+data.houses[0].id)
                }
                else {
                    console.error("Not implemented: Multiple houses")
                }
            }
        ).catch(
            (error)=>{
                console.log(error);
                //alert(error);
            }
        );
    }

    render() {
        return <p>Loading...</p>
        //Todo: implement this
    }
}