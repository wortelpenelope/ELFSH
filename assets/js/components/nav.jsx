import React from 'react';
import {Link} from 'react-router-dom';

import {localize} from '../util/localization.js';

export class Nav extends React.Component {
    render() {
        return <div className="nav">
            <div className={"nav-element " + (this.props.selected === "lijst" ? "nav-element-selected" : "")}>
                <Link to={"/" + this.props.user_id + "/list/" + this.props.house_id}>
                    {localize("nav_list")}
                </Link>
            </div>

            <div className={"nav-element " + (this.props.selected === "cost" ? "nav-element-selected" : "")}>
                <Link to={"/" + this.props.user_id + "/cost/" + this.props.house_id}>
                    {localize("nav_cost")}
                </Link>
            </div>

            <div className={"nav-element " + (this.props.selected === "settings" ? "nav-element-selected" : "")}>
                <Link to={"/" + this.props.user_id + "/settings/" + this.props.house_id}>
                    {localize("nav_settings")}
                </Link>
            </div>
        </div>
    }
}