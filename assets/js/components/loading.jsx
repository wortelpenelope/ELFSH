import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome/index.es";
import {localize} from "../util/localization";

export class FullscreenLoading extends React.Component {
  render() {
    return <React.Fragment>
      <FontAwesomeIcon icon="circle-notch" spin/>
      {localize("loading")}
    </React.Fragment>
  }
}

export let InlineLoading = FullscreenLoading;