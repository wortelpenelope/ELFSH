//TODO: use this module
import {get_cookie, set_cookie} from './cookies.js';

var localization = {
  //errors
  "err404": {
    "en": "404 - page not found",
    "nl": "404 - pagina niet gevonden"
  },
  "error_invalid_response": {
    "en": "Invalid server response",
    "nl": "Bericht van server niet begrepen"
  },
  "loading": {
    "en": "Loading...",
    "nl": "Laden..."
  },
  'back': {
    'en': 'Back',
    'nl': 'Terug',
  },
  "error_not_authenticated_1": {
    "en": "Authentication failed, please ",
    "nl": "Authenticatie mislukt, probeer opniew "
  },
  "error_not_authenticated_2": {
    "en": "log in",
    "nl": "in te loggen",
  },
  "error_not_authenticated_3": {
    "en": " again",
    "nl": ""
  },
  //dates
  "date_weekday": {
    "en": [
      "Sun",
      "Mon",
      "Tue",
      "Wen",
      "Thu",
      "Fri",
      "Sat"
    ],
    "nl": [
      "zo",
      "ma",
      "di",
      "wo",
      "do",
      "vr",
      "za",
    ]
  },
  //voor header
  "header_welcome": {
    "en": "Welcome, ",
    "nl": "Welkom, ",
  },
  "header_notify": {
    "en": "Enable notifications",
    "nl": "Notificaties toestaan"
  },
  "header_logout": {
    "en": "Log out",
    "nl": "Uitloggen"
  },
  //voor footer
  "footer_line_1": {
    "en": "ELFSH - Maurits van Riezen. Still many bugs, please report them.",
    "nl": "ELFSH - Maurits van Riezen. Nog veel bugs en spelfouten; graag aan mij melden."
  },
  "footer_line_2": {
    "en": "View source and contribute at ", //extra space is important
    "nl": "Bekijk bron en draag een steentje bij op ",
  },
  //For navigation
  "nav_list": {
    "en": "List",
    "nl": "Lijst"
  },
  "nav_cost": {
    "en": "Costs",
    "nl": "Kosten"
  },
  "nav_settings": {
    "en": "Settings",
    "nl": "Instellingen"
  },
  //for login screen
  "login_welcome": {
    "en": "welcome to ELFSH",
    "nl": "welkom bij ELFSH"
  },
  "login_please": {
    "en": "Please log in",
    "nl": "Log alstublieft in"
  },
  "login_username": {
    "en": "username",
    "nl": "gebruikersnaam",
  },
  "login_password": {
    "en": "password",
    "nl": "wachtwoord"
  },
  "login_password_confirm": {
    "en": "Confirm password",
    "nl": "Bevestig wachtwoord"
  },
  "login_create_account": {
    "en": "Not a member yet? Create an account",
    "nl": "Nog geen lid? Maak nu een account"
  },
  "login_forgot_password": {
    "en": "Forgot password?",
    "nl": "Wachtwoord vergeten?"
  },
  "login_login": {
    "en": "Sign in!",
    "nl": "Inloggen"
  },
  //The ELFSH marketing description
  "promo_title": {
    "en": "What is ELFSH?",
    "nl": "Wat is ELFSH?"
  },
  "promo_p_1": {
    "en": "Are you tired of messaging people every day about who wants to arange food? Would you rather" +
    " not send payment requests for every communual expense when you know you'll probably have to pay" +
    " another one the next day? Do you also feel like green and white make a great color combination?" +
    " Then ELFSH is the app for you!",
    "nl": "Ben je het zat elke dag te appen wie zin heeft om te koken? Wil je liever niet 10 keer per dag" +
    " een tikkie sturen of steeds weer betalen voor dezelfde persoon? Vind je groen op wit ook zo'n mooie kleurcombinatie?" +
    " Dan is ELFSH echt iets voor jouw of jouw huis."
  },
  "promo_header_2": {
    "en": "Keep track of who cooks and who eats",
    "nl": "Houd bij wie waneer mee eet"
  },
  "promo_p_3": {
    "en": "With on tap on your phone, everybody knows you want food tonight. The cookingpoint system makes" +
    " it easy to decide who will be responsible. On only 10 seconds you can notify your housemates who will" +
    " be cooking. Want to bring guests? Easily add another person and the costs and points will be distributed" +
    " fairly automatically.",
    "nl": "Tik s'ochtens op je telefoon makkelijk even op een knop, en het is geregeld. Heb je tijd om te koken?" +
    "Zet jezelf op K binnen een seconde. ELFSH houd ook kookpunten bij, dus waneer niemand zin heeft eerlijk" +
    "besloten kan worden wie de sjaak is. Ook kan je heel makkelijk +1 aanklikken om gasten mee te nemen, en" +
    "kookpunten en gasten worden automatish geredistrubuteert."
  },
  "promo_header_4": {
    "en": "Keep track of expenses",
    "nl": "Houd kosten bij",
  },
  "promo_p_5": {
    "en": "Bought toilet paper for the house? Put it on EFLSH in a few minutes. ELFSH will keep track of the" +
    " balance of every member, so you can either pay everything bulk sum, or just buy some extra stuff when" +
    " your balance becomes too low.",
    "nl": "Krat bier voor het huis gekocht? Zet het in een paar minuten op ELFSH. ELFSH houd je saldo bij zodat" +
    "je niet steeds betaalverzoeken hoeft te sturen. Kosten worden automatish eerlijk verdeeld over de" +
    "gebruikers."
  },
  "promo_header_6": {
    "en": "Manage users and invite new people",
    "nl": "Beheer gebruikers en nodig mensen uit"
  },
  "promo_p_7": {
    "en": "Easily add new people to your house via email. A house can have as many people as you like," +
    "while a person can also be a member of multiple houses. ",
    "nl": "Je kan gemmakelijk niewe mensen aan je huis toevoegen. Ze krijgen een uitnodiginglink per email." +
    "Een persoon kan met hetzelfe account deel zijn van meerdere huizen, en kan moeiteloos switchen, en" +
    "een huis kan zoveel mensen hebben als je wil."
  },
  "promo_header_8": {
    "en": "Interest? Contact us!",
    "nl": "Interesse? Neem contact op!"
  },
  "promo_p_9": {
    "nl": "Is ELFSH ook wat voor u huis? Voorlopig kan u een account voor u huis maken door ons een email" +
    "te sturen naar " +
    "\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x40\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x2e\x6e\x6c",
    "en": "Is ELFSH something for your house? Contact us and we will create a acount for your house. Send" +
    " us an email at" +
    " \x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x40\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x2e\x6e\x6c" +
    ""
  },
  "promo_faq": {
    "nl": "Veel gestelde vragen",
    "en": "Frequently asked questions"
  },
  "promo_faq_1": {
    "nl": "Disclaimer: Dit zijn geen echt vaak gestelde vragen, alleen vragen die ik anticipeer vaak te horen",
    "en": "Disclaimer: These are not actually frequently asked questions, just questions I anticipate to" +
    " hear often."
  },
  "promo_faq_q_1": {
    "nl": "Waarom is ELFSH beter dan eetlijst.nl?",
    "en": "Why is ELFSH better than eetlijst.nl?",
  },
  "promo_faq_a_1": {
    "nl": "Somige van de voordelen:",
    "en": "The advantages include:"
  },
  "promo_faq_a_1_b": {
    "nl": [
      "Account per persoon, dus ex-huisgenoten kunnen niet met de kosten klooien.",
      "Beter onderhouden, voldoet aan de internet internet standaarden van 2019.",
      "Sneller dankzij efficentere communicatie met server.",
      "Open-source: Vertrouw je mij niet met je data? Clone the source en je kan hosten op je eigen server."
    ],
    "en": [
      "Available in English and in other timezones",
      "Account per person instead of per house: Easily deny access to specific people",
      "Future-proof: Follows the internet standards of 2019",
      "Very fast and responsive due to client-side rendering and intelligent caching",
      "Open source: You can close the repo and run your own server if you want to."
    ]
  },
  "promo_faq_q_2": {
    "nl": "Ik vind ELFSH een lelijke naam",
    "en": "I think ELFSH is a ugly name"
  },
  "promo_faq_a_2": {
    "nl": "Dit is geen vraag, maar ik zal hem toch beantwoorden. Veel mensen vinden dit. Heb je een " +
    " beter idee, dan hoor ik het graag.",
    "en": "Lots of people agree with you, so I have decided to change the name. Please contact me with" +
    " any suggestions."
  },

  //Voor reset scherm
  "reset_title": {
    "en": "Reset Password",
    "nl": "Wachtwoord Resetten"
  },
  "reset_explain": {
    "en": "Forgotten your password? Enter your email and a password reset link will be sent to your email. This link " +
    "will stay valid a limited amount of time.",
    "nl": "Je wachtwoord vergeten? Voer u email in en er zal en reset link naar u toe gemailt worden. " +
    "Deze link is gelimiteerde tijd geldig."
  },
  //Voor kosten scherm
  "cost_submit": { //also used in settings
    "en": "Submit!",
    "nl": "Versturen!"
  },
  "cost_cancel": {
    "en": "Cancel",
    "nl": "Annuleer"
  },
  "cost_description": {
    "en": "Description: ",
    "nl": "Beschrijving: "
  },
  "cost_amount": {
    "en": "Amount: ",
    "nl": "Gemaakte kosten: "
  },
  "cost_payed_by": {
    "en": "Payed by: ",
    "nl": "Betaald door: ",
  },
  "cost_payed_for": {
    "en": "Participants: ",
    "nl": "Deelnemers: "
  },
  "cost_deleted": {
    "en": "[Deleted]",
    "nl": "[Verwijderd]",
  },
  "cost_add_expense": {
    "en": "Add expense",
    "nl": "Kosten toevoegen"
  },
  //For list
  "list_money": {
    "en": "Money",
    "nl": "Geld"
  },
  "list_point": {
    "en": "Cooking points",
    "nl": "Kookpunten"
  },
  "list_confirm": {
    "en": "The closing time has passed. While it is still possible to sign in, please confirm with whoever is" +
    "cooking to make sure there is enough food for you.",
    "nl": "De sluitingstijd is verstreken. U kunt u nog inschrijven, maar het is mischien handig om even te " +
    "overleggen met de koker of er genoeg eten voor je is."
  },
  "list_confirm_ok": {
    "en": "Confirm",
    "nl": "Bevestig"
  },
  "list_confirm_cancel": {
    "en": "Cancel",
    "nl": "Annuleer"
  },
  //For settings
  "settings_title": {
    "en": "Settings for ",
    "nl": "Settings for ",
  },
  "settings_general": {
    "en": "General",
    "nl": "Algemeen"
  },
  "settings_name": {
    "en": "House name: ",
    "nl": "Naam huis: ",
  },
  "settings_endtime": {
    "en": "Closing time: ",
    "nl": "Sluitingstijd: ",
  },
  "settings_dayseditable": {
    "en": "Editable time: ",
    "nl": "Dagen bewerkbaar: "
  },
  "settings_timezone": {
    "en": "timezone: ",
    "nl": "tijd zone:"
  },
  "settings_users": {
    "en": "Users",
    "nl": "Gebruikers"
  },
  "settings_invitations": {
    "en": "Invitations",
    "nl": "Uitnodigingen"
  },
  "settings_user_name": {
    "en": "Name: ",
    "nl": "Naam: ",
  },
  "settings_user_email": {
    "en": "Email: ",
    "nl": "Email: "
  },
  "settings_user_lastlogin": {
    "en": "Last log-in",
    "nl": "Laatst ingelogd"
  },
  "settings_user_remove": {
    "en": "Remove",
    "nl": "Verwijder"
  },
  "settings_user_send": {
    "en": "Send invitation",
    "nl": "Verstuur uitnodiging"
  },
  "setting_user_language": {
    "en": "Language: ",
    "nl": "Taal: ",
  },
  "setting_lang_dutch": {
    "en": "Dutch",
    "nl": "Nederlands"
  },
  "setting_lang_english": {
    "en": "English",
    "nl": "Engels"
  },
  "setting_never": {
    "en": "Never",
    "nl": "Nooit"
  },

  //Settings for invitation
  "invitation_match_1": {
    "en": "You must login to the account matching the email '",
    "nl": "Om deze uitnodiging te accepteren, moet u inloggen met het emailadres dat deze uitnodiging ontvangen heeft"
  },
  "invitation_match_2": {
    "en": " in order to accept this invitation",
    "nl": ""
  },

  "invitation_logout_title": {
    "en": "This invitation is not accessible from this account",
    "nl": "Deze uitnodiging is niet bechikbaar voor dit account"
  },
  "invitation_logout_1": {
    "en": "This invitation is linked to the email'",
    "nl": "Deze uitnodiging is gelinkt aan het emailadres '"
  },
  "invitation_logout_2": {
    "en": "'. This email is already associated with an account, thus only" +
    "that account can accept this invitation",
    "nl": "'. Alleen dit account mag deze uitnodiging accepteren"
  },
  "invitation_logout_3": {
    "en": "You may accept this invitation by signing out of this account, and" +
    "into the correct account. It is also possible to reset the account password" +
    "if you do not remember it.",
    "nl": "U kunt deze uitnidiging toch accepteren door bij dit account uit te loggen" +
    "en bij het andere account in te loggen."
  },
  "invitation_logout": {
    "en": "Sign out",
    "nl": "Uitloggen"
  },
  "invitation_confirm_welcome": {
    "en": "Welcome back to ELFSH",
    "nl": "Welkom terug bij ELFSH"
  },
  "invitation_confirm_1": {
    "en": "You are about to join '",
    "nl": "U staat op het punt u aan te sluiten bij '"
  },
  "invitation_confirm_2": {
    "en": "'. Confirm?",
    "nl": "'. Bevestigen?"
  },
  "invitation_confirm": {
    "en": "Confirm",
    "nl": "Bevestigen"
  },
  "invitation_done_title": {
    "en": "All set!",
    "nl": "Alles geregeld!"
  },
  "invitation_done_link": {
    "en": "Continue",
    "nl": "Doorgaan"
  },
  "invitation_invalid_page": {
    "en": "Page not found: ",
    "nl": "Pagina niet gevonden:  "
  },

  "err_internal": {
    "en": "An internal error occured",
    "nl": "Een interne fout is opgetreden"
  },

  "err_explain": {
    "en": "Sorry for the inconveniance. Please report the error to error to " +
    "\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x40\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x2e\x6e\x6c" +
    ". Please include the technical details displayed below, as well as your browser, operating system," +
    " and anything else unusual about your setup.",
    "nl": "Onze excuzes. Zou u alstublieft deze fout willen melden naar " +
    "\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x40\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x2e\x6e\x6c" +
    ". Vermed in u email ook de gehele technishe detais hieronder, en welke browser en bestuuringsystem u " +
    "gebuikt."
  },

  "err_details": {
    "en": "Technical details",
    "nl": "Technishe details"
  },

  // For new houes
  "newh_welcome": {
    en: 'Welcome',
    nl: 'Welkom',
  },
  "newh_captcha_title": {
    en: 'First, please verify you are human:',
    nl: 'Eerst, even bewijzen dat je een mens bent:'
  },
  "newh_captcha_label": {
    en: 'Characters above:',
    nl: 'de karakters hier boven:'
  },
  "newh_basic_title": {
    en: 'Basic settings',
    nl: 'Basis Instellingen'
  },
  "newh_house_name": {
    en: 'House name',
    nl: 'Naam Huis'
  },
  "newh_personal_title": {
    en: 'Personal Settings',
    nl: 'Gebruikers Instellingen'
  },

  "newh_licence_title": {
    en: 'Licence Agreement',
    nl: 'Voorwaarden'
  },

  "newh_licence_1": {
    en: ' I accept that ELFSH is not' +
    ' liable for any damage caused by' +
    ' the software',
    nl: 'Ik accepteer dat ELFSH niet verantwoordelijk is voor shade door' +
    'de software'
  },
  "newh_licence_2": {
    en: 'I accept that my account may be terminated at any time for' +
    ' any reason without warning',
    nl: 'Ik accepteer dat mijn account op ieder moment voor elke rede' +
    'mag worden beëindigd'
  },
  "newh_licence_4": {
    en: 'I accept that these' +
    ' conditions may be changed any time for' +
    ' any reason without warning',
    nl: 'Ik accepteer dat deze voorwaaren op ieder moment mogen worden' +
    'verandert zonder waarschuwing'
  },
  "newh_licence_5": {
    en: ' If I find a security vulnerability, I will immediately' +
    ' report it and not abuse it',
    nl: ' In het geval dat ik een veligheidsfout ontdek, zal ik die' +
    'onmiddelijk raporteren en die niet misbruiken.'
  },
  "newh_captcha_validation": {
    en: "Please fill in the captcha"
  },
  "newh_username_validation": {
    en: "Please enter a username",
  },
  "newh_housename_validation": {
    en: "Please enter a name for your house/group"
  },
  "newh_password_validation": {
    en: "Please enter a password"
  },
  "newh_password2_validation": {
    en: "Please confirm your password"
  },
  "newh_password2_validation_nomatch": {
    en: "Passwords do not match"
  },
  "newh_email_validation": {
    en: "Please enter a valid email address"
  },
  "newh_licence_validation": {
    en: "You must accept the licence agreement"
  },
  "newh_accept_title": {
    en: "Welcome to ELFSH",
    nl: "Welkom bij ELFSH"
  },
  "newh_accept_text": {
    en: "A email has been sent containing a confirmation link to finalize creating your account? Did not get an email?" +
    " Check your spam folder. The email will be sent from ",
    nl: "Een email is verstuurd met een link om u account te activeren? Geen email gekeregen? Check u spam map. De" +
    " email zal verstuurd worden vanaf "
  }
};

export function setLanguage(lang) {
  set_cookie("lang", lang);
  let l = document.getElementsByTagName("html")[0];
  l.lang = lang;
  localStorage.setItem("lang", lang);
}

export function localize(name) {
  let language = localStorage.getItem("lang");
  return localization[name] === undefined ? `[Error translating ${name}]` : localization[name][language];
}

export function formatMoney(amount) {
  let n = ("" + (Number(amount) / 100)).split(".");
  if (n.length === 1) {
    return "€" + n[0] + ".-";
  }
  else if (n[1].length === 2) {
    return "€" + n[0] + "." + n[1];
  } else {
    return "€" + n[0] + "." + n[1] + "0";
  }
}