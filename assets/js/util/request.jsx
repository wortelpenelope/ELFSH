import {get_cookie} from "./cookies";
import React from 'react';
import {Link} from 'react-router-dom';
import {localize} from './localization.js';

export function formatError(error) {
    if (error instanceof Error){
        return ""+error;
    }
    else {
        return error;
    }
}

export function request(url, data=undefined){
    let p;
    if (data!==undefined) {
        p = fetch(
            url,
            {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': get_cookie('csrftoken'),
                },
                "credentials": "same-origin",
            }
        )
    }
    else {
        p = fetch(
            url,
            {
                "credentials": "same-origin"
            }
        )
    }
    return p.then(
        (response) => {
            //TODO: test if response.ok is actually supported in the correct brouwsers
            if (!response.ok) {
                return response.text()
                    .then((error)=>{
                        if (response.status !== 403 || error.indexOf("CSRF")!==-1) {
                            return Promise.reject(new Error(error))
                        }
                        else {
                            return Promise.reject(
                                <span>
                                    {localize("error_not_authenticated_1")}
                                    <Link to="/logout">
                                        {localize("error_not_authenticated_2")}
                                    </Link>
                                    {localize("error_not_authenticated_3")}
                                </span>);
                        }
                    });
            }
            else {
                return response.json();
            }
        }
    )
}