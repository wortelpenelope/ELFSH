import React from 'react';

export class MoneyInput extends React.Component {
    constructor(props) {
        super();
        this.state = {
            lastValue: "" + props.value / 100,
            isValid: true,
        }
    }

    render() {
        return <input type={"number"}
                      step="0.01"
                      placeholder="€0.00"
                      value={this.state.isValid ? this.props.value / 100 : this.state.lastValue}
                      onKeyPress={
                          (event) => {
                              if (!event.key.match(/[0-9.]/g)) {
                                  event.preventDefault();
                              }
                          }
                      }
                      onChange={
                          (event) => {
                              this.setState({lastValue: event.target.value});
                              if (event.target.value.length !== 0) {
                                  this.setState({isValid: true});
                                  let val = Math.round(Number.parseFloat(event.target.value) * 100);
                                  this.props.onChange(val)
                              }
                              else {
                                  this.setState({isValid: false});
                              }
                          }
                      }
        />
    }
}