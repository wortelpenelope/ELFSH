import psycopg2
import psycopg2.errors
import sqlite3
import re


def copydata():
    psconn = psycopg2.connect("dbname=elfsh user=elfshuser password=qD20@hKS!3$C host=mousetail.nl")
    litecon = sqlite3.connect("db_prod_18_11_15_12_14.sqlite3")

    try:
        litecurr = litecon.cursor()

        tables = litecurr.execute("SELECT name FROM main.sqlite_master WHERE type='table';").fetchall()

        print(tables)
        tables = [i[0] for i in tables if 'sqlite' not in i[0]]

        for table in tables:
            litecurr.execute("SELECT * FROM \"" + table + "\"")

            columnames = re.findall(r"\"[^\"]+\"",
                       litecurr.execute(
                           "SELECT sql FROM main.sqlite_master WHERE type='table' AND NAME=?",
                           (table,))[0])
            print(columnames)

            allvalues = litecurr.fetchall()
            if len(allvalues) == 0:
                continue
            else:
                print("Migrating table " + table)

            length = len(allvalues[0])
            parameterstring = "(" + ",".join("%s" for i in range(length)) + ")"

            try:
                with psconn:
                    with psconn.cursor() as cscurr:
                        cscurr.executemany(
                            "INSERT INTO \"{}\" VALUES {}".format(table, parameterstring),
                            allvalues
                        )
            except psycopg2.errors.UniqueViolation as ex:
                print(ex)
            except psycopg2.errors.ForeignKeyViolation as ex:
                print(table + " depends on " + str(ex).split("\"")[-2])
                tables.append(table)  # Do it again at the end
            except psycopg2.errors.InvalidDatetimeFormat as ex:
                print("Column order wrong for table " + table)
                print(ex)
    finally:
        psconn.close()
        litecon.close()


if __name__ == "__main__":
    copydata()
