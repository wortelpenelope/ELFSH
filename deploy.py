import sys, subprocess


def run(arguments):
    print("\t>" + " ".join(arguments))
    process = subprocess.Popen(arguments, universal_newlines=True)
    process.wait()
    if process.returncode != 0:
        raise SystemError("Non-zero return code: " + str(process.returncode))
    sys.stderr.write("\n")
    print()


def deploy():
    print("Getting new files from git...")
    run(["git", "checkout", "."])
    run(["git", "pull"])
    print("Updating JS...")
    run(["npm", "ci"])
    print("Running JS...")
    run(["npm", "run", "release"])
    print("Updating python...")
    run(["pip", "install", "-r", "requirements.txt"])
    print("Setting application in release mode...")
    with open("ELFSH/settings.py", "r") as f:
        data = f.read()
    with open("ELFSH/settings.py", "w") as f:
        for line in data.split("\n"):
            if line.startswith("DEBUG ="):
                f.write("DEBUG = False\n")
            else:
                f.write(line + "\n")

    print("Running migrations...")
    run(["python", "manage.py", "migrate"])
    print("Collectings static files...")
    run(["python", "manage.py", "collectstatic", "--noinput"])
    print()
    print("DONE!")
    print("Don't forget to hit 'reload' in the python-anywhere control panel")
    print()


if __name__ == "__main__":
    if input("Are you sure you want to deploy? (Y/N)").upper() in ("Y", "YES"):
        deploy()
