const path = require('path');
const webpack = require('webpack');
let BundleTracker = require('webpack-bundle-tracker');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  mode: "development",
  entry: {//"index":"./assets/js/app",
    "vendor": "./assets/js/index",
    "serviceworker": "./assets/js/serviceworker/serviceworker.js",
    "notifications": "./assets/js/notifications/notifications.js"
  },

  output: {
    path: path.resolve('./static/assets/bundles/'),
    filename: '[name]-[hash].js',
    //globalObject: "this",
  },

  plugins: [
    new BundleTracker(
      {filename: './webpack-stats.json'}
    ),
  ],

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          //plugins: ['@babel/plugin-proposal-object-rest-spread']
        }
      }
    ]
  },
  resolve: {
    modules: ['node_modules', 'bower_components'],
    extensions: ['.js', '.jsx'],
    alias: {
      "@fortawesome/fontawesome-free-solid$": "@fortawesome/fontawesome-free-solid/shakable.es.js",
      "@fortawesome/fontawesome-free-regular$": "@fortawesome/fontawesome-free-regular/shakable.es.js"
    }
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  }
};